﻿using DataBase;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public class ProductService
    {
        private BaseDbContext db = new BaseDbContext();

        public void AddToDb(Product product)
        {
            db.Products.Add(product);
            db.SaveChanges();
        }


        public void RemoveFromDbbyId(int id)
        {
            var zm=db.Products.First(x => x.Id == id);
            db.Products.Remove(zm);
            db.SaveChanges();
        }

        public Product GetProductById (int? id)
        {
            return db.Products.FirstOrDefault(x => x.Id == id);
        }

        public void EditProduct (Product product)
        {
            db.Entry(product).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public List<Product> GetAllProducts()
        {
            return db.Products.ToList();
        }

        

    }
}
