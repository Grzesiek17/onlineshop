﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBase;
using Models;

namespace Application
{
    public class OrderService
    {
        private BaseDbContext db = new BaseDbContext();

        public void AddToDb(Order order)
        {
            db.Orders.Add(order);
            db.SaveChanges();
        }


        public void RemoveFromDbbyId(int id)
        {
            var zm = db.Orders.First(x => x.Id == id);
            db.Orders.Remove(zm);
            db.SaveChanges();
        }

        public Order GetOrderById(int? id)
        {
            return db.Orders.FirstOrDefault(x => x.Id == id);
        }

        public void EditOrder(Order order)
        {
            db.Entry(order).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public List<Order> GetAllOrders()
        {
            return db.Orders.ToList();
        }
    
    }
}
