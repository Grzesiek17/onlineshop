﻿using System;
using System.Data.Entity;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using Application;
using Models;

namespace OnlineShop.Tests
{
    [TestClass]
    public class ProductDbActionsTest
    {
        private static int productId = 867;

        private static readonly Product testProduct = new Product()
        {
            Id = productId,
            Name = "testProduct",
            Price = 9.99,
        };

        [OneTimeSetUp]
        public void Init()
        {
            
        }

        [SetUp]
        public void Setup()
        {
            var dbContext = new ProductService();
//            dbContext.DeleteProductById(productId);
            dbContext.RemoveFromDbbyId(productId);
        }

        [TestMethod]
        public void AddedProductShouldBeInDatabase()
        {
            var dbContext = new ProductService();

            var addedProduct = testProduct;
            dbContext.AddToDb(addedProduct);

            var addedProductInBase = dbContext.GetAllProducts().Exists(p => p == addedProduct);

            Assert.IsTrue(addedProductInBase);
        }

        [TestMethod]
        public void DatabaseShouldNotBeEmpyIfProductAdded()
        {
            var dbContext = new ProductService();

            var addedProduct = new Product()
            {
                Name = "Test Product 1",
                Price = 10.99,
                Id = productId
            };
            dbContext.AddToDb(addedProduct);

            var allProducts = dbContext.GetAllProducts();

            Assert.IsNotNull(allProducts);
            Assert.IsNotEmpty(allProducts);
        }

        [TestMethod]
        public void NewContextShouldNotContainTestProduct()
        {
            var dbContext = new ProductService();

            Assert.IsNull(dbContext.GetProductById(productId));
        }

        public void EditedProductShouldDiffer()
        {
//            var dbContext = new ProductService();
//            dbContext.AddToDb(testProduct);
//            var currentProduct = testProduct;
//
//            var productWithChanges = new Product()
//            {
//                Id = productId,
//                Name = "Changed Product",
//                Price = 9.99,
//            };
//
//
//            var changedProduct = dbContext.GetProductById(productId);
        }


        public void ShouldNotContainDeletedProduct()
        {
//           var dbContext = new ProductService();
//            var addedProduct = testProduct;
//
//            dbContext.AddToDb(addedProduct);
//            Assert.True(dbContext.GetAllProducts().Exists(p => p == addedProduct));
//
//            dbContext.RemoveFromDbbyId(productId);
//            
//            Assert.IsNull(dbContext.GetProductById(productId));

        }

        [OneTimeTearDown]
        public void CleanUp()
        {
            Setup();
        }
    }
}
