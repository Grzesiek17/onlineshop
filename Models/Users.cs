﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Users : Entity
    {
        public string Login { get; set; }
        public bool isBan { get; set; }

        public string Ban
        {
            get
            {
                if (isBan)
                {
                    return "Yes";
                }
                else
                {
                    return "No";
                }
            }
        }
    }
}
