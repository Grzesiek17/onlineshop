﻿namespace Models
{
    public class Product : Entity
    {
        public bool isAvaible { get; set; }
        public string Name { get; set; }

        public double Price { get; set; }

        public string Availability

        {
            get
            {
                if (isAvaible)
                {
                    return "Yes";
                }
                else
                {
                    return "No";
                }
            }
        }
    }
}