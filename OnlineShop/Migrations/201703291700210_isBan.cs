namespace OnlineShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isBan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "isBan", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "isBan");
        }
    }
}
