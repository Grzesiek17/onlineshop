﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataBase;
using Models;
using OnlineShop.Models;
using System.Data.Entity.Validation;

namespace OnlineShop.Controllers
{
    public class UsersController : Controller
    {
        private ApplicationDbContext appDbContext = new ApplicationDbContext();

        // GET: Users
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            List<ApplicationUser> usersList = appDbContext.Users.ToList();
            return View(usersList);
        }

        // GET: Users/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = appDbContext.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = appDbContext.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user); // poprawic widok - zmienic model
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, UserName, isBan")] ApplicationUser user) // moze nie dzialac!
        {
            if (ModelState.IsValid)
            {

                appDbContext.Entry(user).State = EntityState.Modified;
                appDbContext.SaveChanges();
                return RedirectToAction("Index");

            }
            return View(user); // poprawic widok
        }

        // GET: Users/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = appDbContext.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var user = appDbContext.Users.Find(id);
            appDbContext.Users.Remove(user);
            appDbContext.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                appDbContext.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
