﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Application;
using DataBase;
using Models;

namespace OnlineShop.Controllers
{
    public class OrdersController : Controller
    {
//        private BaseDbContext db = new BaseDbContext();
        private OrderService dbContext = new OrderService();

        // GET: Orders
        public ActionResult Index()
        {
            return View(dbContext.GetAllOrders());
        }

        // GET: Orders/Details/5
        [Authorize(Roles = "Admin, Buyer")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = dbContext.GetOrderById(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductsList = order.ProductList;
            return View(order);
        }

        // GET: Orders/Create
        [Authorize(Roles = "Admin, Buyer")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Date,User")] Order order)
        {
            order.Date = DateTime.Now;
            order.User = System.Web.HttpContext.Current.User.Identity.Name;

            if (ModelState.IsValid)
            {
                dbContext.AddToDb(order);
                return RedirectToAction("Index");
            }

            return View(order);
        }

        // GET: Orders/Edit/5
        [Authorize(Roles = "Admin, Buyer")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = dbContext.GetOrderById(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Date,User")] Order order)
        {
            if (ModelState.IsValid)
            {
                dbContext.EditOrder(order);
                return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        [Authorize(Roles = "Admin, Buyer")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = dbContext.GetOrderById(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            dbContext.RemoveFromDbbyId(id);
            return RedirectToAction("Index");
        }
    }
}
